import requests

from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city}, {state}", "per_page": 1}

    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    pics = response.json()

    pics_url = pics["photos"][0]["src"]["original"]
    return pics_url


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)

    coordinates = response.json()

    lat = coordinates[0]["lat"]
    lon = coordinates[0]["lon"]

    wurl = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"

    wresponse = requests.get(wurl)
    weather = wresponse.json()

    if not weather:
        return None

    temp = weather["main"]["temp"]
    wdesc = weather["weather"][0]["description"]

    #    print(temp)
    #    print(wdesc)
    return {"temperature": temp, "description": wdesc}
